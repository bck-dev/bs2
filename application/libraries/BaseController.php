<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class BaseController extends CI_Controller {
	
	protected $userId = '';
	protected $name = '';
	protected $global = array ();
		
	function isLoggedIn() {
		$isLoggedIn = $this->session->userdata ( 'loggedIn' );
		
		if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
			redirect ( 'login' );
		} else {
			$this->userId = $this->session->userdata ( 'userId' );
			$this->name = $this->session->userdata ( 'name' );
			
			$this->global ['name'] = $this->name;
		}
	}

	function isAdmin() {
		$userType = $this->session->userdata ( 'userType' );
		
		if ( $userType != 'admin') {
			redirect ( 'profile' );
		} 
	}
	
	function isUser(){
		$userType = $this->session->userdata ( 'userType' );
		
		if ( $userType != 'user') {
			redirect ( 'admin' );
		} 
	}
	
}