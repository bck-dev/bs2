<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['login'] = 'login';
$route['checkLogin'] = 'login/checkLogin';
$route['register'] = 'register';
$route['registerUser'] = 'register/save';
$route['regMsg'] = 'register/regMsg';
$route['logout'] = 'login/logout';
$route['forgot'] = 'forgot';
$route['sendForgotMail'] = 'forgot/sendForgotMail';
$route['checkMail'] = 'forgot/checkMail';
$route['resetPassword/(:num)/(:any)'] = 'forgot/resetPassword/$1/$2';
$route['updateNewPassword'] = 'forgot/updateNewPassword';
$route['resetSuccess'] = 'forgot/resetSuccess';

//profile route
$route['profile'] = 'profile';
$route['loadProfile'] = 'profile/loadProfile';
$route['updateProfile'] = 'profile/updateProfile';
$route['book/new'] = 'book';
$route['book/save'] = 'book/save';
$route['book/single/(:num)'] = 'book/single/$1';
$route['book/myBooks'] = 'book/myBooks';

// admin route
$route['admin'] = 'admin/admin';
$route['admin/category'] = 'admin/category';
$route['admin/category/add'] = 'admin/category/add';
$route['admin/category/delete/(:num)'] = 'admin/category/delete/$1';
$route['admin/category/loadUpdate/(:num)'] = 'admin/category/loadUpdate/$1';

// ajax calls
$route['ajaxEmail'] = 'register/email';