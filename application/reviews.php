<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/banner'); ?>

<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

<div class="container py-7">
  <div class="row row-eq-height">
    <div class="col-lg-6 mt-4">
      <div class="fb-post w-100" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/"></div>
    </div>
    <div class="col-lg-6 mt-4">
      <div class="fb-post w-100" data-href="https://www.facebook.com/nicolaborland/posts/10160243083065463"></div>
    </div>
    <div class="col-lg-6 mt-4">
      <div class="fb-post w-100" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/"></div>
    </div>
    <div class="col-lg-6 mt-4">
      <div class="fb-post w-100" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/"></div>
    </div>
    <!-- <?php foreach($reviews as $review): ?>
          <div class="col-md-6">
            <div class="card review mb-5">
              <div class="card-body">
                <h5 class="card-title"><?php echo $review->title; ?></h5>
                <h6 class="card-subtitle mb-2 text-muted">
                  Reviewed by:</span> <span class="purple mr-4"> <?php echo $review->name; ?></span> 
                  on:<span class="purple"> <?php echo $review->date; ?></span>
                </h6>
                <?php $whole=floor($review->rating); $deci=$review->rating - $whole; ?>
                <span class="dark_purple">
                  <?php $i=1; while($i<=$whole): ?>
                    <i class="fas fa-star"></i>
                  <?php $i++; endwhile; ?>
                  <?php if($deci>0): ?>
                    <i class="fas fa-star-half-alt"></i>
                  <?php endif; ?>
                </span>
                <p class="card-text"><?php echo $review->review; ?></p>
                <h6 class="card-subtitle mb-2 text-muted">
                  <?php if($review->type=="Location"): ?>                
                    Medical Center : <?php echo $review->lname; ?>
                  <?php endif; ?>   
                  <?php if($review->type=="Doctor"): ?>                
                    Doctor : <?php echo $review->dname; ?> 
                  <?php endif; ?>
                </h6>
              </div>
            </div>
              
              
          </div>
          
      <?php endforeach; ?> -->
  </div>
</div>

<?php $this->load->view('components/common/footer'); ?>