<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class UserBook extends CI_Model
{
    public function myBooks($userId) {
        $this->db->select('user_has_books.id, user_has_books.price, user_has_books.cover, book.title, book.category');
        $this->db->where('userId', $userId);
        $this->db->from('user_has_books');
        $this->db->join('book', "user_has_books.bookId = book.id");
        return $this->db->get()->result();
    }
}