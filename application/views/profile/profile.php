<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?>   

<section class="my_account_area pt--80 pb--55 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <div class="my__account__wrapper">
                    <form action="<?php echo base_url('updateProfile') ?>" method="post">
                        <div class="account__form">
                            <h3 class="account__title mb-3">Update Profile</h3>
                            <?php if($msg):?><div class="success text-center mb-3"><?php echo $msg; ?></div><?php endif; ?>
                            <div class="input__box">
                                <label>Full Name<span>*</span></label>
                                <input type="text" name="fullName" value="<?php echo $profile->fullName ?>" required>
                                <input type="hidden" name="id" value="<?php echo $profile->id ?>">
                            </div>
                            <div class="input__box">
                                <label>Address<span>*</span></label>
                                <input type="text" name="address" value="<?php echo $profile->address ?>" required>
                            </div>
                            <div class="input__box">
                                <label>Telephone<span>*</span></label>
                                <input type="text" name="telephone" value="<?php echo $profile->telephone ?>" required>
                            </div>
                            <div class="form__btn">
                                <button type="submit" class="mt-2">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('components/common/footer'); ?>