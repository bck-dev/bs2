<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?>   

<section class="my_account_area pt--80 pb--55 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <div class="my__account__wrapper">
                    <form action="<?php echo base_url('book/'); echo $action; ?>" method="post"  enctype="multipart/form-data">
                        <div class="account__form">
                            <h3 class="account__title mb-3">Add New Book</h3>
                            <div class="input__box">
                                <label>Title<span>*</span></label>
                                <input type="text" name="title" value="<?php ?>" required>
                            </div>
                            <div class="input__box">
                                <label>Description<span>*</span></label>
                                <textarea class="text" name="description" required><?php ?></textarea>
                            </div>
                            <div class="input__box w-50 float-left pr-3">
                                <label>Author<span>*</span></label>
                                <input type="text" name="author" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Category<span>*</span></label>
                                <select name="category" class="select" required>
                                    <option selected disabled>Select Category</option>
                                    <?php foreach($categories as $c): ?>
                                        <option><?php echo $c->category ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input__box w-50 float-left pr-3">
                                <label>Price<span>*</span></label>
                                <input type="text" name="price" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Pages<span>*</span></label>
                                <input type="text" name="pages" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left pr-3">
                                <label>Year<span>*</span></label>
                                <input type="text" name="year" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Condition<span>*</span></label>
                                <select name="condition" class="select" required>
                                    <option selected disabled>Select Condition</option>
                                    <option>Mint Condition</option>
                                    <option>Good Condition</option>
                                    <option>Average Condition</option>
                                </select>
                            </div>
                            <div class="input__box">
                                <label>Cover<span>*</span></label>
                                <input type="file" name="cover" id="cover" class="pt-2" required/>
                            </div>
                            <div class="form__btn">
                                <button type="submit" class="mt-2">Add Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('components/common/footer'); ?>