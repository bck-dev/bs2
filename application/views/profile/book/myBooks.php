<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?>   

<section class="my_account_area pt--80 pb--55 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <div class="my__account__wrapper">
                    <div class="account__form">
                        <h3 class="account__title mb-3">My Books</h3>
                        <div class="row">
                            <?php foreach($myBooks as $book): ?>
                                <div class="col-lg-4 text-center">
                                    <img src="<?php echo base_url(); ?>/<?php echo $book->cover; ?>" class="w-100"/>
                                    <label class="m-0 p-0 mt-3"><b><?php echo $book->title; ?></b></label><br />
                                    <label class="m-0 p-0"><?php echo $book->category; ?></label><br />
                                    <label class="m-0 p-0"><b><?php echo number_format($book->price, 2); ?></b></label><br />
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('components/common/footer'); ?>