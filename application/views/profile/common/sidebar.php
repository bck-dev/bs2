<div class="col-lg-3 col-12">
    <h3 class="account__title mb-3">Side Bar</h3>
    <ul>
        <li><a href="<?php echo base_url('profile') ?>">Dashboard</a></li>
        <li><a href="<?php echo base_url('loadProfile') ?>">Update Profile</a></li>
        <li><a href="<?php echo base_url('book/new') ?>">Add New Book</a></li>
        <li><a href="<?php echo base_url('book/myBooks') ?>">View My Books</a></li>
        <li><a href="<?php echo base_url('logout') ?>">Logout</a></li>
    </ul>
</div>