<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
	<!-- Start Bradcaump area -->
	<div class="ht__bradcaump__area bg-login">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="bradcaump__inner text-center">
						<h2 class="bradcaump-title">Login</h2>
						<nav class="bradcaump-content">
							<a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
							<span class="brd-separetor">/</span>
							<span class="breadcrumb_item active">Login</span>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Bradcaump area -->
	<!-- Start My Account Area -->
	<section class="my_account_area pt--80 pb--55 bg--white">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12 m-auto">
					<div class="my__account__wrapper">
						<h3 class="account__title text-center">Login</h3>
						<?php if($message):?><div class="error text-center"><?php echo $message; ?></div><?php endif; ?>
						<form action="<?php echo base_url('checkLogin') ?>" method="post">
							<div class="account__form">
								<div class="input__box">
									<label>Email<span>*</span></label>
									<input type="email"	name="email" required>
								</div>
								<div class="input__box">
									<label>Password<span>*</span></label>
									<input type="password" name="password" required>
								</div>
								<div class="form__btn">
									<button type="submit" class="mt-2">Login</button>
								</div>
								<a class="forget_pass" href="<?php echo base_url('register'); ?>">Not yet registered? Register now!</a>
								<a class="forget_pass mt-0" href="<?php echo base_url('forgot'); ?>">Lost your password?</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End My Account Area -->
        
<?php $this->load->view('components/common/footer'); ?>