<div class="col-6 p-3">
<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Category</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($allData as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->category; ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/category/loadUpdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/category/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>
</div>
</div>