<!doctype html>
<html class="no-js" lang="zxx">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Home | Books Store</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/template/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/template/images/icon.png">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800"
		rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/plugins.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/custom.css">

	<script src="<?php echo base_url() ?>assets/template/js/vendor/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>assets/template/js/vendor/modernizr-3.5.0.min.js"></script>
</head>

<body>