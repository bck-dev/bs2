<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();      
        $this->isAdmin();     
    }

    public function index()
    {
        $allData = $this->common->getAllData('category');

        $data=['pageName'=>"Categories",
                'action'  => 'add',
                'allData' => $allData, 
                ];

        $this->load->view('dashboard/category',$data);

    }

    public function add()
    {
        $data = array
        (
            'category' => $_POST['category'],
        );  

        $this->db->insert('category', $data);

        $allData = $this->common->getAllData('category');

        $pageData=['pageName'=>"Categories",
                    'action'  => 'add',
                    'allData' => $allData, 
        ];

        $this->load->view('dashboard/category',$pageData);

    }

    public function delete($id)
    {

        $this->common->delete('category', $id);
    
        $allData = $this->common->getAllData('category');
    
        $pageData=['pageName'=>"Categories",
                'allData' => $allData,
                'action'  => 'add'
            ];
            
        $this->load->view('dashboard/category', $pageData); 
    
    }
    
    public function loadUpdate($id){
    
        $updateData = $this->common->getById('category',$id);
    
        $allData = $this->common->getAllData('category');
    
        $pageData=['pageName'=>"Categories",
                    'allData' => $allData,
                    'updateData'  => $updateData,
                    'action'  => 'update',
                ];
    
        $this->load->view('dashboard/category', $pageData);
    
    }
    
    public function update($id){
        
        $data = array
        (
        'category' => $_POST['category']
        ); 
    
        $this->common->update('category', $id, $data);
        $allData = $this->common->getAllData('category');
    
        $pageData=['pageName'=>"Categories",
            'allData' => $allData,
            'action'  => 'add'
        ];
    
        $this->load->view('dashboard/category', $pageData);
    
    }


} 

?>