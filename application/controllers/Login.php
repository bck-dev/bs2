<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
    $this->load->view('login');
  }

  public function checkLogin()
	{
    $email =  $_POST['email'];
    $password =  $_POST['password'];
   
    $result = $this->user->userLogin($email,$password);

    if($result!=FALSE){
      $user=$this->common->getByFeild('user', 'email', $email);
      $sessionArray = array('userId' => $user[0]->id,
                            'userType' => $user[0]->userType,
                            'loggedIn' => TRUE );

      $this->session->set_userdata($sessionArray);
      
      if($user[0]->userType=="admin"){
        redirect ( 'admin' );
      }
      else{
        redirect ( 'profile' );
      }
    }
    else{
      $data= array('message'=>'Incorrect email or password!' );
      
      $this->load->view('login', $data);
    }
  }

  public function logout(){
    $this->session->sess_destroy ();	
		redirect ( 'login' );
  }
}

?>