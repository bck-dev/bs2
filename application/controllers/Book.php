<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Book extends BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();      
		$this->isUser();  
	}

	public function index()
	{ 
		$categories = $this->common->getAllData('category');
		$data = array('pageName' => "Add New Book",
			'categories' => $categories,
			'action' => "save"
		);

		$this->load->view('profile/book/new', $data);
	}

  	public function save(){

		$author = $this->input->post('author');
		if($this->common->getCount('author', 'name', $author)==0){
			$authorData = array('name' => $author);
			$this->db->insert('author', $authorData);

			$authorId = $this->db->insert_id();
		}
		else{
			$authors = $this->common->getByFeild('author', 'name', $author);
			$authorId = $authors['0']->id;
		}

		$title = $this->input->post('title');
		if($this->common->getCount('book', 'title', $title)==0){
			$bookData = array('title' => $title,
				'category' => $this->input->post('category'),
				'authorId' => $authorId
			);

			$this->db->insert('book', $bookData);
			$bookId = $this->db->insert_id();
		}
		else{
			$books = $this->common->getByFeild('book', 'title', $title);
			$bookId = $books['0']->id;
		}
	
		$cover = $this->common->upload('cover');

		$data = array(
			'bookId' => $bookId,
			'userId' => $this->session->userdata('userId'),
			'description' => $this->input->post('description'),
			'price' => $this->input->post('price'),
			'pages' => $this->input->post('pages'),
			'year' => $this->input->post('year'),
			'condition' => $this->input->post('condition'),
			'addedDate' => date('Y-m-d'),
			'status' => "Available",
			'cover' => $cover
		);

		$this->db->insert('user_has_books', $data);
		$bookCopyId = $this->db->insert_id();
		redirect('book/single/'.$bookCopyId);
	}

	public function single($bookCopyId){
		$bookCopy = $this->common->getById('user_has_books', $bookCopyId);
		$book = $this->common->getById('book', $bookCopy->bookId);
		$author = $this->common->getById('author', $book->authorId);

		$data = array('pageName' => "My Book",
			'bookCopy' => $bookCopy,
			'book' => $book,
			'author' => $author
		);

		$this->load->view('profile/book/single', $data);
	}

	public function myBooks(){
		$myBooks = $this->userBook->mybooks($this->session->userdata ('userId'));

		$data = array('pageName' => "My Book",
			'myBooks' => $myBooks
		);

		$this->load->view('profile/book/myBooks', $data);
	}

}