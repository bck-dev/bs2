<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController {

  public function __construct()
  {
    parent::__construct();
    $this->isLoggedIn();      
    $this->isUser();  
  }

  public function index()
  {
    $userId = $this->session->userdata ( 'userId' );
    $profile = $this->common->getByForigenKey('profile', 'userId', $userId);

    $data = array('pageName' => "My Profile",
      'profile' => $profile    
    );

    if($profile->fullName){
      $page = 'profile/home';
    }
    else{
      $page = 'profile/profile';
    }
    
    $this->load->view($page, $data);
  }
  
  public function loadProfile()
  {
    $userId = $this->session->userdata ( 'userId' );
    $profile = $this->common->getByForigenKey('profile', 'userId', $userId);

    $data = array('pageName' => "My Profile",
      'profile' => $profile    
    );
    
    $this->load->view('profile/profile', $data);
  }

  public function updateProfile()
  {
    $id = $this->input->post ( 'id' );

    $updateData = array ('fullName' => $this->input->post('fullName'),
      'address' => $this->input->post('address'),
      'telephone' => $this->input->post('telephone'),
    );

    $this->common->update('profile', $id, $updateData);

    $profile = $this->common->getById('profile', $id);

    $data = array('pageName' => "My Profile",
      'profile' => $profile,
      'msg' => "Profile Updated Successfully!" 
    );
    
    $this->load->view('profile/profile', $data);
  }

} 

?>